#pragma once
#include <vector>

using namespace std;

class Solution {
public:
	vector<int> comVec;
	double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
		size_t total_len = nums1.size() + nums2.size();
		vector<int>::iterator it1 = nums1.begin();
		vector<int>::iterator it2 = nums2.begin();
		while (it1 != nums1.end() && it2 != nums2.end())
		{
			if (*it1 < *it2)
			{
				comVec.push_back(*it1);
				it1++;
			}
			else
			{
				comVec.push_back(*it2);
				it2++;
			}
		}
		while (it1 != nums1.end())
		{
			comVec.push_back(*it1);
			it1++;
		}
		while (it2 != nums2.end())
		{
			comVec.push_back(*it2);
			it2++;
		}
		if (total_len % 2)
		{
			return (double)comVec[total_len / 2];
		}
		else
		{
			return ((double)comVec[total_len / 2] + comVec[total_len / 2 - 1]) / 2;
		}
	}
};

void testPuzzle4()
{
	printf("Once Passed, No Test Case Needed.\n");
}