#pragma once
#include <string>

using namespace std;

class Solution {
public:
	int vis[256] = { 0 };
	int lengthOfLongestSubstring(string s) {
		int cur = 0, total_len = s.length();
		int max_len = 0;
		memset(vis, -1, sizeof(vis));
		for (int i = 0; i < total_len; i++)
		{
			if (vis[s[i]] == -1)
			{
				cur++;
				vis[s[i]] = i;
				if (max_len < cur)
				{
					max_len = cur;
				}
			}
			else
			{
				cur = 0;
				i = vis[s[i]];
				memset(vis, -1, sizeof(vis));
			}
		}
		return max_len;
	}
};

void testPuzzle3()
{
	Solution puzzle3;
	printf("%d", puzzle3.lengthOfLongestSubstring("abcabcbb"));
}
