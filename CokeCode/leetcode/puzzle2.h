#pragma once
#include <vector>

using std::vector;
/**
* Definition for singly-linked list.
* struct ListNode {
*     int val;
*     ListNode *next;
*     ListNode(int x) : val(x), next(NULL) {}
* };
*/

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
public:
	ListNode * addTwoNumbers(ListNode* l1, ListNode* l2) {
		ListNode *ans = new ListNode(0);
		ListNode *tmpNode = ans, *comNode = nullptr;
		int tmpVal = 0;
		while (l1 != nullptr && l2 != nullptr) {
			tmpVal = l1->val + l2->val + tmpNode->val;
			tmpNode->val = tmpVal % 10;
			if (l1->next != nullptr || l2->next != nullptr || tmpVal / 10 > 0)
			{
				tmpNode->next = new ListNode(tmpVal / 10);
			}
			tmpNode = tmpNode->next;
			l1 = l1->next;
			l2 = l2->next;
		}
		if (l1 != nullptr) {
			comNode = l1;
		}
		else if (l2 != nullptr)
		{
			comNode = l2;
		}
		while (comNode != nullptr)
		{
			tmpVal = tmpNode->val + comNode->val;
			tmpNode->val = tmpVal % 10;
			if (comNode->next != nullptr || tmpVal / 10)
			{
				tmpNode->next = new ListNode(tmpVal / 10);
			}
			comNode = comNode->next;
			tmpNode = tmpNode->next;
		}
		return ans;
	}

	ListNode * makeListNode(vector<int> numbers)
	{
		ListNode* node = new ListNode(numbers.back());
		ListNode* tmpNode = node;
		numbers.pop_back();
		while (!numbers.empty())
		{
			tmpNode->next = new ListNode(numbers.back());
			tmpNode = tmpNode->next;
			numbers.pop_back();
		}
		return node;
	}

	void printListNode(ListNode *node)
	{
		printf("[");
		while (node->next != nullptr)
		{
			printf("%d->", node->val);
			node = node->next;
		}
		printf("%d]\n", node->val);
	}
};

void testPuzzle2()
{
	Solution puzzle2;
	vector<int> inputs;
	char tchar;
	ListNode *l1(0), *l2(0);
	while (scanf("%c", &tchar))
	{
		if (tchar >= '0' && tchar <= '9')
		{
			inputs.push_back(tchar - '0');
		}
		if (tchar == ' ')
		{
			l1 = puzzle2.makeListNode(inputs);
			puzzle2.printListNode(l1);
			inputs.clear();
		}
		if (tchar == '\n')
		{
			l2 = puzzle2.makeListNode(inputs);
			puzzle2.printListNode(l2);
			break;
		}
	}
	puzzle2.printListNode(puzzle2.addTwoNumbers(l1, l2));
}
